let activeModeIndex = 0;
const roundModes = [
	{
		value: 3,
		linkedButton: ""
	},
	{
		value: 5,
		linkedButton: ""
	},
	{
		value: 7,
		linkedButton: ""
	},
	{
		value: 9,
		linkedButton: ""
	}
]

const playModes = [
	{name: 'rock', beats: 'scissors'},
	{name: 'paper', beats: 'rock'},
	{name: 'scissors', beats: 'paper'}
];

let gameActive = false;

let currentRound = 1;

let playerOneWins = 0;
let playerTwoWins = 0;

const roundSelectionForm = document.querySelector('.round-selection');
const playSelectionForm = document.querySelector('.play-selection');

roundModes.forEach((option, index) => {
	const templateContent = document.createElement('template');
	templateContent.innerHTML = `
    <button onclick="setActiveMode('${index}')">
		Best of ${option.value}
    </button>
	`;

	roundSelectionForm.appendChild(templateContent.content);

	option.linkedButton = roundSelectionForm.querySelectorAll("button")[index];
});


// Create a button for each play mode option
playModes.forEach((option) => {
	const templateContent = document.createElement('template');
	templateContent.innerHTML = `
		<button name="play-mode" 
			onclick="playRound('${option.name}', playModes[Math.floor(Math.random() * playModes.length)].name)">
			${option.name}
		</button>
  	`;

	playSelectionForm.appendChild(templateContent.content);
});

const setActiveMode = (newActive) => {
	const setActive = element => {
		element.style.boxShadow = "0 0 12px white";
		element.style.background = "rgb(55 55 55)"
	}

	const setInactive = element => {
		element.style.boxShadow = "none";
		element.style.background = "initial"
	}

	setInactive(roundModes[activeModeIndex].linkedButton);
	setActive(roundModes[newActive].linkedButton);

	activeModeIndex = newActive;
}

let winner;
const playRound = (playerOneChoice, playerTwoChoice) => {

	const result = {
		player1: playerOneChoice,
		player2: playerTwoChoice,
		winner: ''
	};

	for (let i = 0; i < playModes.length; i++) {
		const choice = playModes[i];

		if (choice.name === playerOneChoice && choice.beats === playerTwoChoice) {
			result.winner = 'Player 1';
			playerOneWins++;

		} else if (choice.name === playerTwoChoice && choice.beats === playerOneChoice) {
			result.winner = 'Player 2';
			playerTwoWins++;
		}
	}

	if (result.winner === '') {
		result.winner = 'Tie';
	}


	if (currentRound >= roundModes[activeModeIndex].value) {
		setGameState(2);
	}
	currentRound++;
	winner = result.winner;
	updatePage();
}

const updatePage = () => {
	document.querySelector(".match-status h3").textContent = `
		${playerOneWins}:${playerTwoWins}
	`;

	document.querySelector(".round-info").textContent = `ROUND ${currentRound}`;

	document.querySelector("#result h2").textContent = `${winner} won!!`
}

setActiveMode(0);

const preMatchInterface = document.querySelector("#pre-match");
const matchInterface = document.querySelector("#match");
const resultInterface = document.querySelector("#result");

const setGameState = (newGameState) => {

	switch (newGameState) {
		case 0:
			playerOneWins = playerTwoWins = 0;
			currentRound = 1;
			gameActive = false;

			preMatchInterface.style.display = "block";
			matchInterface.style.display = "none";
			resultInterface.style.display = "none";
			break;
		case 1:
			preMatchInterface.style.display = "none";
			matchInterface.style.display = "block";
			break;
		case 2:
			playerOneWins = playerTwoWins = 0;
			gameActive = false;

			preMatchInterface.style.display = "none";
			matchInterface.style.display = "none";
			resultInterface.style.display = "block";
			break;
	}
	updatePage();
}
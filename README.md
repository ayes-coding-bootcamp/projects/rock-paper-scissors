![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/ayes-coding-bootcamp/projects/rock-paper-scissors?branch=master&style=for-the-badge)  ![GitLab](https://img.shields.io/gitlab/license/ayes-coding-bootcamp/projects/rock-paper-scissors?label=license&style=for-the-badge)  ![GitLab Release (latest by date)](https://img.shields.io/gitlab/v/release/ayes-coding-bootcamp/projects/rock-paper-scissors?date_order_by=created_at&sort=date&style=for-the-badge)

---

# 🪨📰✂️ Rock Paper Scissors Game

This is a simple rock paper scissors game implemented in JavaScript.

Link to the game:
https://ayes-coding-bootcamp.gitlab.io/projects/rock-paper-scissors/

---

### Description

The game consists of two main parts: selecting the number of rounds and playing the game. The player can choose to play
the game in the best of 3, 5, 7, or 9 rounds. After selecting the number of rounds, the player can then choose to play
one of three modes: rock, paper, or scissors.

The game keeps track of the number of rounds played and the number of wins for each player. When the selected number of
rounds is reached, the game displays the winner.



---

### How to play

1. Choose the number of rounds to play by clicking one of the "Best of" buttons.

2. Choose a play mode (rock, paper, or scissors) by clicking one of the buttons.

3. The game will automatically choose a random play mode for the second player.

4. The winner of the round is determined based on the rules of rock paper scissors (rock beats scissors, paper beats
   rock, scissors beats paper).

5. After each round, the score is updated and displayed.

6. The game ends when the selected number of rounds is reached and the winner is displayed.

---

### Code Overview

The game is implemented in JavaScript and consists of several variables and functions.

##### Variables

- `activeModeIndex`: Keeps track of the active game mode (best of 3 or best of 5).

- `roundModes`: An array of game modes, each with a `value` (number of rounds) and a `linkedButton` property that
  references the button associated with that game mode.

- `playModes`: An array of play modes, each with a `name` and a `beats` property that defines what it beats.

- `gameActive`: A boolean that tracks whether the game is currently active.

- `currentRound`: An integer that tracks the current round number.

- `playerOneWins`: An integer that tracks the number of wins for player one.

- `playerTwoWins`: An integer that tracks the number of wins for player two.

- `roundSelectionForm`: A reference to the form that contains the round mode buttons.

- `playSelectionForm`: A reference to the form that contains the play mode buttons.

- `winner`: A string that tracks the winner of the current round.

##### Functions

- `setActiveMode`: Sets the active game mode and updates the UI.

- `playRound`: Plays a round of the game and updates the score and UI.

- `updatePage`: Updates the UI with the current score and round information.

- `setGameState`: Sets the current game state (pre-match, match, or result) and updates the UI.

---

### Credits

Main Developer: Justin Schildt